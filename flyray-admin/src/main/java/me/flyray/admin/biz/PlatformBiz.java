package me.flyray.admin.biz;

import me.flyray.admin.entity.Platform;
import me.flyray.admin.mapper.PlatformMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.flyray.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:11:27 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class PlatformBiz extends BaseBiz<PlatformMapper, Platform> {

}
