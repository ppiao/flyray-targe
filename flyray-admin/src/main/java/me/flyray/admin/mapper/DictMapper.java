package me.flyray.admin.mapper;

import me.flyray.admin.entity.Dict;
import tk.mybatis.mapper.common.Mapper;

public interface DictMapper extends Mapper<Dict> {
}