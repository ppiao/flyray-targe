package me.flyray.admin.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.flyray.admin.biz.*;
import me.flyray.admin.entity.Element;
import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.Menu;
import me.flyray.admin.entity.ResourceAuthority;
import me.flyray.admin.entity.UserRole;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.vo.AuthorityMenuTree;
import me.flyray.admin.vo.MenuTree;
import me.flyray.auth.common.config.UserAuthConfig;
import me.flyray.auth.common.util.jwt.IJWTInfo;
import me.flyray.auth.common.util.jwt.JWTHelper;
import me.flyray.common.rest.BaseController;
import me.flyray.common.util.TreeUtil;

import tk.mybatis.mapper.entity.Example;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2017-06-12 8:49
 */
@Controller
@RequestMapping("menu")
public class MenuController extends BaseController<MenuBiz, Menu> {
    @Autowired
    private UserBiz userBiz;
    @Autowired
    private UserRoleBiz userRoleBiz;
    @Autowired
    private UserAuthConfig userAuthConfig;
    @Autowired
    private ResourceAuthorityBiz resourceAuthorityBiz;
    @Autowired
    private ElementBiz elementBiz;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Menu> list(String title) {
        Example example = new Example(Menu.class);
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        return baseBiz.selectByExample(example);
    }

    /**
     * 查询菜单下的按钮
     * @param limit
     * @param offset
     * @param name
     * @param menuId
     * @return
     */
    @RequestMapping(value = "/element", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<Element> page(@RequestParam(defaultValue = "10") int limit,
                                             @RequestParam(defaultValue = "1") int offset, String name, @RequestParam(defaultValue = "0") int menuId) {
        Example example = new Example(Element.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("menuId", menuId);
        if(StringUtils.isNotBlank(name)){
            criteria.andLike("name", "%" + name + "%");
        }
        List<Element> elements = elementBiz.selectByExample(example);
        return new TableResultResponse<Element>(elements.size(), elements);
    }

    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse getTree(@RequestParam Map<String, Object> param) {
        Example example = new Example(Menu.class);
        String title = (String) param.get("title");
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        List<MenuTree> list = getMenuTree2(baseBiz.selectByExample(example), AdminCommonConstant.ROOT,request.getHeader(userAuthConfig.getTokenHeader()));
        return BaseApiResponse.newSuccess(list);
    }



    @RequestMapping(value = "/system", method = RequestMethod.GET)
    @ResponseBody
    public List<Menu> getSystem() {
        Menu menu = new Menu();
        menu.setParentId(AdminCommonConstant.ROOT);
        return baseBiz.selectList(menu);
    }

    @RequestMapping(value = "/menuTree", method = RequestMethod.GET)
    @ResponseBody
    public List<MenuTree> listMenu(Integer parentId) {
        try {
            if (parentId == null) {
                parentId = this.getSystem().get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        Example example = new Example(Menu.class);
        Menu parent = baseBiz.selectById(parentId);
        example.createCriteria().andLike("path", parent.getPath() + "%").andNotEqualTo("id",parent.getId());
        return getMenuTree(baseBiz.selectByExample(example), parent.getId());
    }

    @RequestMapping(value = "/authorityTree", method = RequestMethod.GET)
    @ResponseBody
    public List<AuthorityMenuTree> listAuthorityMenu() {
        List<AuthorityMenuTree> trees = new ArrayList<AuthorityMenuTree>();
        AuthorityMenuTree node = null;
        for (Menu menu : baseBiz.selectListAll()) {
            node = new AuthorityMenuTree();
            node.setText(menu.getTitle());
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees, String.valueOf(AdminCommonConstant.ROOT));
    }

    @RequestMapping(value = "/user/authorityTree", method = RequestMethod.GET)
    @ResponseBody
    public List<MenuTree> listUserAuthorityMenu(Integer parentId){
    	String userId = userBiz.getUserByUsername(getCurrentUserName()).getUserId();
        try {
            if (parentId == null) {
                parentId = this.getSystem().get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        return getMenuTree(baseBiz.getUserAuthorityMenuByUserId(userId),parentId);
    }

    @RequestMapping(value = "/user/system", method = RequestMethod.GET)
    @ResponseBody
    public List<Menu> listUserAuthoritySystem() {
        int userId = userBiz.getUserByUsername(getCurrentUserName()).getId();
        return baseBiz.getUserAuthoritySystemByUserId(userId);
    }

    private List<MenuTree> getMenuTree(List<Menu> menus,int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            node.setLabel(menu.getTitle());
            trees.add(node);
        }
        return TreeUtil.bulid(trees,String.valueOf(root)) ;
    }
    
    private List<MenuTree> getMenuTree2(List<Menu> menus,int root,String token) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        String roleId = "0";
        String userId = BaseContextHandler.getXId();
        UserRole userRole = new UserRole();
        userRole.setUserId(userId);
        List<UserRole>  userRoleList = userRoleBiz.selectList(userRole);
        UserRole userRoleOther = userRoleList.get(0);
        roleId = userRoleOther.getRoleId();
        ResourceAuthority resourceAuthority = new ResourceAuthority();
        resourceAuthority.setAuthorityId(String.valueOf(roleId));
        resourceAuthority.setResourceType("menu");
        List<ResourceAuthority> resourceAuthorityList = resourceAuthorityBiz.selectList(resourceAuthority);
    	for (Menu menu : menus) {
       	 //判断一下有没有这个菜单的权限
        	if(!"1".equals(roleId)){
	           	for (ResourceAuthority ra : resourceAuthorityList) {
	   				if(menu.getId() == Integer.valueOf(ra.getResourceId())){
	   		            node = new MenuTree();
	   		            BeanUtils.copyProperties(menu, node);
	   		            node.setLabel(menu.getTitle());
	   		            trees.add(node);
	   		            break;
	   				}
	   			}
          }else {
        	  node = new MenuTree();
        	  BeanUtils.copyProperties(menu, node);
        	  node.setId(String.valueOf(menu.getId()));
        	  node.setParentId(String.valueOf(menu.getParentId()));
        	  node.setLabel(menu.getTitle());
        	  trees.add(node);
          }
    	}
    return TreeUtil.bulid(trees,root) ;
    }


}
