package me.flyray.admin.vo;

import me.flyray.common.vo.TreeNode;

/** 
* @author: bolei
* @date：2018年4月16日 下午4:46:33 
* @description：类说明
*/

public class RoleTree extends TreeNode {

	String label;
	String name;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
