package me.flyray.auth.common.bean;

import java.io.Serializable;

import lombok.Data;
import me.flyray.auth.common.util.jwt.IJWTInfo;


/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 登陆用户授权
**/

@Data
public class UserJWTInfo implements IJWTInfo,Serializable {
	
	/**
	 * 登陆用户名称
	 */
    private String XName;

    /**
     * 登陆用户昵称
     */
    private String nickname;
    /**
     * 用户id userId
     */
    private String XId;
    /**
     * 平台编号
     * */
    private String platformId;
    /**
     * 签名盐值
     * */
    private String saltKey;

    /**
     * 用户类型
     * */
    private String userType;
    
    public UserJWTInfo(String platformId, String XName,String nickname, String XId) {
    	this.platformId = platformId;
        this.XName = XName;
        this.nickname = nickname;
        this.XId = XId;
    }

    public UserJWTInfo(String platformId, String XName,String nickname, String XId, String userType) {
        this.platformId = platformId;
        this.XName = XName;
        this.nickname = nickname;
        this.XId = XId;
        this.userType = userType;
    }

    public UserJWTInfo(String platformId, String saltKey) {
		this.platformId = platformId;
		this.saltKey = saltKey;
	}

    @Override
    public String getXId() {
        return XId;
    }

    @Override
    public String getXName() {
        return XName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserJWTInfo jwtInfo = (UserJWTInfo) o;

        if (XName != null ? !XName.equals(jwtInfo.XName) : jwtInfo.XName != null) {
            return false;
        }
        return XId != null ? XId.equals(jwtInfo.XId) : jwtInfo.XId == null;

    }

    @Override
    public int hashCode() {
        int result = XName != null ? XName.hashCode() : 0;
        result = 31 * result + (XId != null ? XId.hashCode() : 0);
        return result;
    }

	@Override
	public String getPlatformId() {
		return platformId;
	}

	@Override
	public String getSaltKey() {
		return saltKey;
	}

	@Override
	public String getClientCode() {
		return null;
	}

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public String getUserType() {
        return userType;
    }
}
