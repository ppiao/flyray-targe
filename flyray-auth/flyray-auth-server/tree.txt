[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------< com.github.wxiaoqi:flyray-auth-server >----------------
[INFO] Building flyray-auth-server 2.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[WARNING] The POM for com.google.code.kaptcha:kaptcha:jar:2.3 is missing, no dependency information available
[INFO] 
[INFO] --- maven-dependency-plugin:3.0.1:tree (default-cli) @ flyray-auth-server ---
[INFO] com.github.wxiaoqi:flyray-auth-server:jar:2.0-SNAPSHOT
[INFO] +- com.github.wxiaoqi:flyray-interface:jar:2.0-SNAPSHOT:compile
[INFO] |  \- tk.mybatis:mapper:jar:3.4.0:compile
[INFO] |     \- javax.persistence:persistence-api:jar:1.0:compile
[INFO] +- com.github.wxiaoqi:flyray-common:jar:2.0-SNAPSHOT:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- org.springframework.boot:spring-boot:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- org.springframework.boot:spring-boot-starter-logging:jar:2.0.0.RELEASE:compile
[INFO] |  |  |  +- ch.qos.logback:logback-classic:jar:1.2.3:compile
[INFO] |  |  |  |  \- ch.qos.logback:logback-core:jar:1.2.3:compile
[INFO] |  |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.10.0:compile
[INFO] |  |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.10.0:compile
[INFO] |  |  |  \- org.slf4j:jul-to-slf4j:jar:1.7.25:compile
[INFO] |  |  +- javax.annotation:javax.annotation-api:jar:1.3.2:compile
[INFO] |  |  \- org.yaml:snakeyaml:jar:1.19:runtime
[INFO] |  +- org.apache.commons:commons-lang3:jar:3.7:compile
[INFO] |  +- com.google.guava:guava:jar:18.0:compile
[INFO] |  +- commons-io:commons-io:jar:1.3.2:compile
[INFO] |  +- com.github.pagehelper:pagehelper:jar:5.0.3:compile
[INFO] |  |  \- com.github.jsqlparser:jsqlparser:jar:1.0:compile
[INFO] |  +- io.jsonwebtoken:jjwt:jar:0.7.0:compile
[INFO] |  +- javax.servlet:javax.servlet-api:jar:3.1.0:compile
[INFO] |  +- org.bouncycastle:bcprov-jdk15on:jar:1.60:compile
[INFO] |  +- commons-beanutils:commons-beanutils:jar:1.8.3:compile
[INFO] |  +- commons-logging:commons-logging:jar:1.2:compile
[INFO] |  +- net.sf.json-lib:json-lib:jar:jdk15:2.4:compile
[INFO] |  |  +- commons-collections:commons-collections:jar:3.2.1:compile
[INFO] |  |  +- commons-lang:commons-lang:jar:2.5:compile
[INFO] |  |  \- net.sf.ezmorph:ezmorph:jar:1.0.6:compile
[INFO] |  +- commons-codec:commons-codec:jar:1.11:compile
[INFO] |  +- commons-httpclient:commons-httpclient:jar:3.1:compile
[INFO] |  +- com.google.zxing:core:jar:3.2.0:compile
[INFO] |  +- com.google.zxing:javase:jar:3.1.0:compile
[INFO] |  +- com.github.ulisesbocchio:jasypt-spring-boot-starter:jar:2.1.0:compile
[INFO] |  |  \- com.github.ulisesbocchio:jasypt-spring-boot:jar:2.1.0:compile
[INFO] |  |     \- org.jasypt:jasypt:jar:1.9.2:compile
[INFO] |  +- com.alibaba:fastjson:jar:1.2.33:compile
[INFO] |  +- io.springfox:springfox-swagger2:jar:2.9.0:compile
[INFO] |  |  +- io.swagger:swagger-annotations:jar:1.5.18:compile
[INFO] |  |  +- io.swagger:swagger-models:jar:1.5.18:compile
[INFO] |  |  +- io.springfox:springfox-spi:jar:2.9.0:compile
[INFO] |  |  |  \- io.springfox:springfox-core:jar:2.9.0:compile
[INFO] |  |  +- io.springfox:springfox-schema:jar:2.9.0:compile
[INFO] |  |  +- io.springfox:springfox-swagger-common:jar:2.9.0:compile
[INFO] |  |  +- io.springfox:springfox-spring-web:jar:2.9.0:compile
[INFO] |  |  +- com.fasterxml:classmate:jar:1.3.4:compile
[INFO] |  |  +- org.slf4j:slf4j-api:jar:1.7.25:compile
[INFO] |  |  +- org.springframework.plugin:spring-plugin-core:jar:1.2.0.RELEASE:compile
[INFO] |  |  +- org.springframework.plugin:spring-plugin-metadata:jar:1.2.0.RELEASE:compile
[INFO] |  |  \- org.mapstruct:mapstruct:jar:1.2.0.Final:compile
[INFO] |  +- javax.validation:validation-api:jar:2.0.1.Final:compile
[INFO] |  +- org.mockito:mockito-core:jar:2.15.0:compile
[INFO] |  |  +- net.bytebuddy:byte-buddy:jar:1.7.10:compile
[INFO] |  |  +- net.bytebuddy:byte-buddy-agent:jar:1.7.10:compile
[INFO] |  |  \- org.objenesis:objenesis:jar:2.6:compile
[INFO] |  \- org.apache.httpcomponents:httpclient:jar:4.5.5:compile
[INFO] |     \- org.apache.httpcomponents:httpcore:jar:4.4.9:compile
[INFO] +- com.github.wxiaoqi:flyray-auth-common:jar:2.0-SNAPSHOT:compile
[INFO] |  \- org.springframework.cloud:spring-cloud-starter-bus-amqp:jar:2.0.0.M7:compile
[INFO] |     +- org.springframework.cloud:spring-cloud-starter-stream-rabbit:jar:2.0.0.RC2:compile
[INFO] |     |  \- org.springframework.cloud:spring-cloud-stream-binder-rabbit:jar:2.0.0.RC2:compile
[INFO] |     |     +- org.springframework.cloud:spring-cloud-stream-binder-rabbit-core:jar:2.0.0.RC2:compile
[INFO] |     |     +- org.springframework.boot:spring-boot-starter-amqp:jar:2.0.0.RELEASE:compile
[INFO] |     |     +- org.springframework.integration:spring-integration-amqp:jar:5.0.3.RELEASE:compile
[INFO] |     |     \- org.springframework.integration:spring-integration-jmx:jar:5.0.3.RELEASE:compile
[INFO] |     \- org.springframework.cloud:spring-cloud-bus:jar:2.0.0.M7:compile
[INFO] |        +- org.springframework.cloud:spring-cloud-stream:jar:2.0.0.RC2:compile
[INFO] |        |  +- org.springframework.boot:spring-boot-starter-validation:jar:2.0.0.RELEASE:compile
[INFO] |        |  +- org.springframework:spring-tuple:jar:1.0.0.RELEASE:compile
[INFO] |        |  |  \- com.esotericsoftware:kryo-shaded:jar:3.0.3:compile
[INFO] |        |  |     \- com.esotericsoftware:minlog:jar:1.3.0:compile
[INFO] |        |  \- org.springframework.integration:spring-integration-tuple:jar:1.0.0.RELEASE:compile
[INFO] |        \- org.springframework.integration:spring-integration-core:jar:5.0.3.RELEASE:compile
[INFO] |           \- io.projectreactor:reactor-core:jar:3.1.5.RELEASE:compile
[INFO] +- org.hibernate.javax.persistence:hibernate-jpa-2.0-api:jar:1.0.1.Final:compile
[INFO] +- org.springframework.boot:spring-boot-starter-undertow:jar:2.0.0.RELEASE:compile
[INFO] |  +- io.undertow:undertow-core:jar:1.4.22.Final:compile
[INFO] |  |  +- org.jboss.logging:jboss-logging:jar:3.3.2.Final:compile
[INFO] |  |  +- org.jboss.xnio:xnio-api:jar:3.3.8.Final:compile
[INFO] |  |  \- org.jboss.xnio:xnio-nio:jar:3.3.8.Final:runtime
[INFO] |  +- io.undertow:undertow-servlet:jar:1.4.22.Final:compile
[INFO] |  |  \- org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:jar:1.0.0.Final:compile
[INFO] |  +- io.undertow:undertow-websockets-jsr:jar:1.4.22.Final:compile
[INFO] |  |  \- org.jboss.spec.javax.websocket:jboss-websocket-api_1.1_spec:jar:1.1.0.Final:compile
[INFO] |  \- org.glassfish:javax.el:jar:3.0.0:compile
[INFO] +- org.springframework.boot:spring-boot-starter-web:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-json:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jdk8:jar:2.9.3:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.9.3:compile
[INFO] |  |  \- com.fasterxml.jackson.module:jackson-module-parameter-names:jar:2.9.3:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-tomcat:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- org.apache.tomcat.embed:tomcat-embed-core:jar:8.5.28:compile
[INFO] |  |  +- org.apache.tomcat.embed:tomcat-embed-el:jar:8.5.28:compile
[INFO] |  |  \- org.apache.tomcat.embed:tomcat-embed-websocket:jar:8.5.28:compile
[INFO] |  +- org.hibernate.validator:hibernate-validator:jar:6.0.7.Final:compile
[INFO] |  +- org.springframework:spring-web:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-beans:jar:5.0.4.RELEASE:compile
[INFO] |  \- org.springframework:spring-webmvc:jar:5.0.4.RELEASE:compile
[INFO] |     +- org.springframework:spring-aop:jar:5.0.4.RELEASE:compile
[INFO] |     \- org.springframework:spring-expression:jar:5.0.4.RELEASE:compile
[INFO] +- org.springframework.boot:spring-boot-starter-actuator:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-actuator-autoconfigure:jar:2.0.0.RELEASE:compile
[INFO] |  |  \- org.springframework.boot:spring-boot-actuator:jar:2.0.0.RELEASE:compile
[INFO] |  \- io.micrometer:micrometer-core:jar:1.0.1:compile
[INFO] |     +- org.hdrhistogram:HdrHistogram:jar:2.1.10:compile
[INFO] |     \- org.latencyutils:LatencyUtils:jar:2.0.3:compile
[INFO] +- org.springframework.cloud:spring-cloud-starter-openfeign:jar:2.0.0.M1:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-starter:jar:2.0.0.M8:compile
[INFO] |  |  +- org.springframework.cloud:spring-cloud-context:jar:2.0.0.M8:compile
[INFO] |  |  \- org.springframework.security:spring-security-rsa:jar:1.0.5.RELEASE:compile
[INFO] |  |     \- org.bouncycastle:bcpkix-jdk15on:jar:1.56:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-openfeign-core:jar:2.0.0.M1:compile
[INFO] |  |  \- org.springframework.boot:spring-boot-starter-aop:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-commons:jar:2.0.0.M8:compile
[INFO] |  |  \- org.springframework.security:spring-security-crypto:jar:5.0.3.RELEASE:compile
[INFO] |  +- io.github.openfeign:feign-core:jar:9.5.1:compile
[INFO] |  +- io.github.openfeign:feign-slf4j:jar:9.5.1:compile
[INFO] |  +- io.github.openfeign:feign-hystrix:jar:9.5.1:compile
[INFO] |  |  \- com.netflix.archaius:archaius-core:jar:0.7.5:compile
[INFO] |  |     \- com.google.code.findbugs:jsr305:jar:3.0.1:runtime
[INFO] |  +- io.github.openfeign:feign-java8:jar:9.5.1:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-starter-netflix-ribbon:jar:2.0.0.M7:compile
[INFO] |  |  +- com.netflix.ribbon:ribbon:jar:2.2.4:compile
[INFO] |  |  |  +- com.netflix.ribbon:ribbon-transport:jar:2.2.4:runtime
[INFO] |  |  |  |  +- io.reactivex:rxnetty-contexts:jar:0.4.9:runtime
[INFO] |  |  |  |  \- io.reactivex:rxnetty-servo:jar:0.4.9:runtime
[INFO] |  |  |  +- javax.inject:javax.inject:jar:1:runtime
[INFO] |  |  |  \- io.reactivex:rxnetty:jar:0.4.9:runtime
[INFO] |  |  |     +- io.netty:netty-codec-http:jar:4.1.22.Final:runtime
[INFO] |  |  |     |  \- io.netty:netty-codec:jar:4.1.22.Final:runtime
[INFO] |  |  |     \- io.netty:netty-transport-native-epoll:jar:4.1.22.Final:runtime
[INFO] |  |  |        +- io.netty:netty-common:jar:4.1.22.Final:runtime
[INFO] |  |  |        +- io.netty:netty-buffer:jar:4.1.22.Final:runtime
[INFO] |  |  |        +- io.netty:netty-transport-native-unix-common:jar:4.1.22.Final:runtime
[INFO] |  |  |        \- io.netty:netty-transport:jar:4.1.22.Final:runtime
[INFO] |  |  |           \- io.netty:netty-resolver:jar:4.1.22.Final:runtime
[INFO] |  |  +- com.netflix.ribbon:ribbon-core:jar:2.2.4:compile
[INFO] |  |  +- com.netflix.ribbon:ribbon-httpclient:jar:2.2.4:compile
[INFO] |  |  |  \- com.netflix.netflix-commons:netflix-commons-util:jar:0.1.1:runtime
[INFO] |  |  +- com.netflix.ribbon:ribbon-loadbalancer:jar:2.2.4:compile
[INFO] |  |  |  \- com.netflix.netflix-commons:netflix-statistics:jar:0.1.1:runtime
[INFO] |  |  \- io.reactivex:rxjava:jar:1.3.6:compile
[INFO] |  \- org.springframework.cloud:spring-cloud-starter-netflix-archaius:jar:2.0.0.M7:compile
[INFO] |     +- org.springframework.cloud:spring-cloud-netflix-archaius:jar:2.0.0.M7:compile
[INFO] |     +- commons-configuration:commons-configuration:jar:1.8:compile
[INFO] |     +- com.fasterxml.jackson.core:jackson-annotations:jar:2.9.0:compile
[INFO] |     \- com.fasterxml.jackson.core:jackson-core:jar:2.9.3:compile
[INFO] +- org.springframework.cloud:spring-cloud-starter-netflix-hystrix:jar:2.0.0.M7:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-netflix-core:jar:2.0.0.M7:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-netflix-ribbon:jar:2.0.0.M7:compile
[INFO] |  +- com.netflix.hystrix:hystrix-core:jar:1.5.12:compile
[INFO] |  +- com.netflix.hystrix:hystrix-serialization:jar:1.5.12:compile
[INFO] |  |  \- com.fasterxml.jackson.module:jackson-module-afterburner:jar:2.9.3:runtime
[INFO] |  +- com.netflix.hystrix:hystrix-metrics-event-stream:jar:1.5.12:compile
[INFO] |  +- com.netflix.hystrix:hystrix-javanica:jar:1.5.12:compile
[INFO] |  |  +- org.ow2.asm:asm:jar:5.0.4:runtime
[INFO] |  |  \- org.aspectj:aspectjweaver:jar:1.8.13:compile
[INFO] |  \- io.reactivex:rxjava-reactive-streams:jar:1.2.1:compile
[INFO] |     \- org.reactivestreams:reactive-streams:jar:1.0.2:compile
[INFO] +- org.springframework.cloud:spring-cloud-starter-netflix-eureka-client:jar:2.0.0.M7:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-netflix-eureka-client:jar:2.0.0.M7:compile
[INFO] |  +- com.netflix.eureka:eureka-client:jar:1.8.6:compile
[INFO] |  |  +- org.codehaus.jettison:jettison:jar:1.3.7:runtime
[INFO] |  |  |  \- stax:stax-api:jar:1.0.1:runtime
[INFO] |  |  +- com.netflix.netflix-commons:netflix-eventbus:jar:0.3.0:runtime
[INFO] |  |  |  +- com.netflix.netflix-commons:netflix-infix:jar:0.3.0:runtime
[INFO] |  |  |  |  +- commons-jxpath:commons-jxpath:jar:1.3:runtime
[INFO] |  |  |  |  +- org.antlr:antlr-runtime:jar:3.4:runtime
[INFO] |  |  |  |  |  +- org.antlr:stringtemplate:jar:3.2.1:runtime
[INFO] |  |  |  |  |  \- antlr:antlr:jar:2.7.7:runtime
[INFO] |  |  |  |  \- com.google.code.gson:gson:jar:2.8.2:runtime
[INFO] |  |  |  \- org.apache.commons:commons-math:jar:2.2:runtime
[INFO] |  |  +- javax.ws.rs:jsr311-api:jar:1.1.1:runtime
[INFO] |  |  +- com.netflix.servo:servo-core:jar:0.10.1:runtime
[INFO] |  |  |  \- com.netflix.servo:servo-internal:jar:0.10.1:runtime
[INFO] |  |  +- com.sun.jersey:jersey-core:jar:1.19.1:runtime
[INFO] |  |  +- com.sun.jersey:jersey-client:jar:1.19.1:runtime
[INFO] |  |  +- com.sun.jersey.contribs:jersey-apache-client4:jar:1.19.1:runtime
[INFO] |  |  +- com.google.inject:guice:jar:4.1.0:runtime
[INFO] |  |  |  \- aopalliance:aopalliance:jar:1.0:runtime
[INFO] |  |  \- com.github.vlsi.compactmap:compactmap:jar:1.2.1:runtime
[INFO] |  |     \- com.github.andrewoma.dexx:dexx-collections:jar:0.2:runtime
[INFO] |  +- com.netflix.eureka:eureka-core:jar:1.8.6:compile
[INFO] |  |  \- org.codehaus.woodstox:woodstox-core-asl:jar:4.4.1:runtime
[INFO] |  |     +- javax.xml.stream:stax-api:jar:1.0-2:runtime
[INFO] |  |     \- org.codehaus.woodstox:stax2-api:jar:3.1.4:runtime
[INFO] |  +- com.netflix.ribbon:ribbon-eureka:jar:2.2.4:compile
[INFO] |  \- com.thoughtworks.xstream:xstream:jar:1.4.9:compile
[INFO] |     +- xmlpull:xmlpull:jar:1.1.3.1:compile
[INFO] |     \- xpp3:xpp3_min:jar:1.1.4c:compile
[INFO] +- org.springframework.cloud:spring-cloud-sleuth-zipkin:jar:2.0.0.M7:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-sleuth-core:jar:2.0.0.M7:compile
[INFO] |  |  +- org.aspectj:aspectjrt:jar:1.8.13:compile
[INFO] |  |  +- io.zipkin.brave:brave:jar:4.16.1:compile
[INFO] |  |  +- io.zipkin.brave:brave-context-log4j2:jar:4.16.1:compile
[INFO] |  |  +- io.zipkin.brave:brave-instrumentation-spring-web:jar:4.16.1:compile
[INFO] |  |  |  \- io.zipkin.brave:brave-instrumentation-http:jar:4.16.1:compile
[INFO] |  |  \- io.zipkin.brave:brave-instrumentation-spring-webmvc:jar:4.16.1:compile
[INFO] |  |     \- io.zipkin.brave:brave-instrumentation-servlet:jar:4.16.1:compile
[INFO] |  +- io.zipkin.zipkin2:zipkin:jar:2.5.0:compile
[INFO] |  +- io.zipkin.reporter2:zipkin-reporter:jar:2.3.3:compile
[INFO] |  +- io.zipkin.reporter2:zipkin-sender-kafka11:jar:2.3.3:compile
[INFO] |  \- io.zipkin.reporter2:zipkin-sender-amqp-client:jar:2.3.3:compile
[INFO] +- org.springframework.amqp:spring-rabbit:jar:2.0.2.RELEASE:compile
[INFO] |  +- org.springframework.amqp:spring-amqp:jar:2.0.2.RELEASE:compile
[INFO] |  +- com.rabbitmq:amqp-client:jar:5.1.2:compile
[INFO] |  +- com.rabbitmq:http-client:jar:1.3.1.RELEASE:compile
[INFO] |  +- org.springframework:spring-context:jar:5.0.4.RELEASE:compile
[INFO] |  +- org.springframework:spring-messaging:jar:5.0.4.RELEASE:compile
[INFO] |  +- org.springframework:spring-tx:jar:5.0.4.RELEASE:compile
[INFO] |  \- org.springframework.retry:spring-retry:jar:1.2.2.RELEASE:compile
[INFO] +- com.alibaba:druid:jar:1.1.0:compile
[INFO] |  +- com.alibaba:jconsole:jar:1.8.0:system
[INFO] |  \- com.alibaba:tools:jar:1.8.0:system
[INFO] +- com.alibaba:druid-spring-boot-starter:jar:1.1.0:compile
[INFO] |  \- org.springframework.boot:spring-boot-autoconfigure:jar:2.0.0.RELEASE:compile
[INFO] +- org.springframework.boot:spring-boot-starter-jdbc:jar:2.0.0.RELEASE:compile
[INFO] |  +- com.zaxxer:HikariCP:jar:2.7.8:compile
[INFO] |  \- org.springframework:spring-jdbc:jar:5.0.4.RELEASE:compile
[INFO] +- mysql:mysql-connector-java:jar:5.1.45:compile
[INFO] +- tk.mybatis:mapper-spring-boot-starter:jar:1.1.2:compile
[INFO] |  +- org.mybatis.spring.boot:mybatis-spring-boot-starter:jar:1.3.0:compile
[INFO] |  |  +- org.mybatis.spring.boot:mybatis-spring-boot-autoconfigure:jar:1.3.0:compile
[INFO] |  |  +- org.mybatis:mybatis:jar:3.4.4:compile
[INFO] |  |  \- org.mybatis:mybatis-spring:jar:1.3.1:compile
[INFO] |  \- tk.mybatis:mapper-spring-boot-autoconfigure:jar:1.1.2:compile
[INFO] +- com.github.pagehelper:pagehelper-spring-boot-starter:jar:1.2.3:compile
[INFO] |  \- com.github.pagehelper:pagehelper-spring-boot-autoconfigure:jar:1.2.3:compile
[INFO] +- joda-time:joda-time:jar:2.9.9:compile
[INFO] +- org.springframework.cloud:spring-cloud-starter-config:jar:2.0.0.M8:compile
[INFO] |  +- org.springframework.cloud:spring-cloud-config-client:jar:2.0.0.M8:compile
[INFO] |  \- com.fasterxml.jackson.core:jackson-databind:jar:2.9.3:compile
[INFO] +- org.springframework.boot:spring-boot-starter-test:jar:2.0.0.RELEASE:test
[INFO] |  +- org.springframework.boot:spring-boot-test:jar:2.0.0.RELEASE:test
[INFO] |  +- org.springframework.boot:spring-boot-test-autoconfigure:jar:2.0.0.RELEASE:test
[INFO] |  +- com.jayway.jsonpath:json-path:jar:2.4.0:test
[INFO] |  |  \- net.minidev:json-smart:jar:2.3:test
[INFO] |  |     \- net.minidev:accessors-smart:jar:1.2:test
[INFO] |  +- junit:junit:jar:4.12:test
[INFO] |  +- org.assertj:assertj-core:jar:3.9.1:test
[INFO] |  +- org.hamcrest:hamcrest-core:jar:1.3:test
[INFO] |  +- org.hamcrest:hamcrest-library:jar:1.3:test
[INFO] |  +- org.skyscreamer:jsonassert:jar:1.5.0:test
[INFO] |  |  \- com.vaadin.external.google:android-json:jar:0.0.20131108.vaadin1:test
[INFO] |  +- org.springframework:spring-core:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-jcl:jar:5.0.4.RELEASE:compile
[INFO] |  +- org.springframework:spring-test:jar:5.0.4.RELEASE:test
[INFO] |  \- org.xmlunit:xmlunit-core:jar:2.5.1:test
[INFO] +- org.springframework.boot:spring-boot-starter-data-redis:jar:2.0.0.RELEASE:compile
[INFO] |  \- org.springframework.data:spring-data-redis:jar:2.0.5.RELEASE:compile
[INFO] |     +- org.springframework.data:spring-data-keyvalue:jar:2.0.5.RELEASE:compile
[INFO] |     |  \- org.springframework.data:spring-data-commons:jar:2.0.5.RELEASE:compile
[INFO] |     +- org.springframework:spring-oxm:jar:5.0.4.RELEASE:compile
[INFO] |     \- org.springframework:spring-context-support:jar:5.0.4.RELEASE:compile
[INFO] +- redis.clients:jedis:jar:2.9.0:compile
[INFO] |  \- org.apache.commons:commons-pool2:jar:2.5.0:compile
[INFO] +- com.google.code.kaptcha:kaptcha:jar:2.3:compile
[INFO] +- com.google.collections:google-collections:jar:1.0-rc2:compile
[INFO] +- com.spring4all:swagger-spring-boot-starter:jar:1.7.0.RELEASE:compile
[INFO] |  +- io.springfox:springfox-swagger-ui:jar:2.8.0:compile
[INFO] |  \- io.springfox:springfox-bean-validators:jar:2.8.0:compile
[INFO] \- org.projectlombok:lombok:jar:1.16.14:provided
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.042 s
[INFO] Finished at: 2018-09-30T18:29:51+08:00
[INFO] ------------------------------------------------------------------------
