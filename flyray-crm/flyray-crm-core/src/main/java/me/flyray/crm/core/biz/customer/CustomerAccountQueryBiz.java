package me.flyray.crm.core.biz.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.enums.InOutFlag;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.*;
import me.flyray.crm.core.mapper.*;
import me.flyray.crm.facade.request.AccountJournalQueryRequest;
import me.flyray.crm.facade.request.AccountQueryRequest;
import me.flyray.crm.facade.request.AccountingOrderInfoReq;
import me.flyray.crm.facade.response.AccountJournalQueryResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.flyray.common.enums.CustomerType;
import me.flyray.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人或商户账户信息查询
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerAccountQueryBiz {

	@Autowired
	private CustomerBaseMapper customerBaseMapper;
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Autowired 
	private PersonalAccountJournalMapper personalAccountJournalMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	
	@Value("${crm.account.balanceSaltValue}")
	private String balanceSaltValue;

	/**
	 * 账户查询接口
	 * 为了方便统一对返回结果处理
	 * 统一返回arrayList
	 */
	public List<CustomerAccountQueryResponse> accountQuery(AccountQueryRequest accountQueryRequest) {
		log.info("个人或者商户账户信息查询-请求参数：{}" + EntityUtils.beanToMap(accountQueryRequest));
		List<CustomerAccountQueryResponse> list = new ArrayList<CustomerAccountQueryResponse>();
		//查询客户信息中的商户号和个人编号
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(accountQueryRequest.getPlatformId());
		customerBase.setCustomerId(accountQueryRequest.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		//商户账户
		if(CustomerType.CUST_MERCHANT.getCode().equals(accountQueryRequest.getCustomerType())){
			MerchantAccount queryMerAccountparam=new MerchantAccount();
			//添加平台约束
			queryMerAccountparam.setPlatformId(accountQueryRequest.getPlatformId());
			queryMerAccountparam.setMerchantId(customerBase.getMerchantId());
			if(!StringUtils.isEmpty(accountQueryRequest.getMerchantType())){
				queryMerAccountparam.setMerchantType(accountQueryRequest.getMerchantType());
			}
			if(!StringUtils.isEmpty(accountQueryRequest.getAccountType())){
				/**
				 * 获取指定账户类型的商户账户信息
				 * */
				queryMerAccountparam.setAccountType(accountQueryRequest.getAccountType());
				MerchantAccount merchantAccount = merchantAccountMapper.selectOne(queryMerAccountparam);
				CustomerAccountQueryResponse customerAccountQueryResponse = new CustomerAccountQueryResponse();
				BeanUtils.copyProperties(merchantAccount,customerAccountQueryResponse);
				list.add(customerAccountQueryResponse);
				return list;
			}else{
				/***
				 * 获取全部的商户账户信息
				 * */
				List<MerchantAccount> merchantAccounts = merchantAccountMapper.select(queryMerAccountparam);
				for(MerchantAccount mer : merchantAccounts){
					CustomerAccountQueryResponse customerAccountQueryResponse = new CustomerAccountQueryResponse();
					BeanUtils.copyProperties(mer,customerAccountQueryResponse);
					list.add(customerAccountQueryResponse);
				}
				return list;
			}

		}
		//个人账户
		if(CustomerType.CUST_PERSONAL.getCode().equals(accountQueryRequest.getCustomerType())){
			PersonalAccount queryPerAccountparam=new PersonalAccount();
			queryPerAccountparam.setPlatformId(accountQueryRequest.getPlatformId());
			queryPerAccountparam.setPersonalId(customerBase.getPersonalId());
			if(!StringUtils.isEmpty(accountQueryRequest.getAccountType())){
				queryPerAccountparam.setAccountType(accountQueryRequest.getAccountType());
				PersonalAccount personalAccount = personalAccountMapper.selectOne(queryPerAccountparam);
				CustomerAccountQueryResponse customerAccountQueryResponse = new CustomerAccountQueryResponse();
				BeanUtils.copyProperties(personalAccount,customerAccountQueryResponse);
				list.add(customerAccountQueryResponse);
			}else{
				List<PersonalAccount> personalAccounts = personalAccountMapper.select(queryPerAccountparam);
				for(PersonalAccount per : personalAccounts){
					CustomerAccountQueryResponse customerAccountQueryResponse = new CustomerAccountQueryResponse();
					BeanUtils.copyProperties(per,customerAccountQueryResponse);
					list.add(customerAccountQueryResponse);
				}
			}
		}
		return list;
	}
	
	/**
	 * 查询账户交易流水
	 * @time 创建时间:2018年9月27日下午3:57:14
	 * @param accountJournalQueryRequest
	 * @return
	 */
	public List<AccountJournalQueryResponse> accountJournalQuery(AccountJournalQueryRequest accountJournalQueryRequest) {
		log.info("个人或者商户查询账户交易流水-请求参数：{}" + EntityUtils.beanToMap(accountJournalQueryRequest));
		List<AccountJournalQueryResponse> list = new ArrayList<AccountJournalQueryResponse>();
		//查询客户信息中的商户号和个人编号
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(accountJournalQueryRequest.getPlatformId());
		customerBase.setCustomerId(accountJournalQueryRequest.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		if(CustomerType.CUST_MERCHANT.getCode().equals(accountJournalQueryRequest.getCustomerType())){
			MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
			merchantAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
			merchantAccountJournalReq.setCustomerId(accountJournalQueryRequest.getCustomerId());
			merchantAccountJournalReq.setMerchantId(customerBase.getMerchantId());
			merchantAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
			List<MerchantAccountJournal> merchantAccountJournals = merchantAccountJournalMapper.select(merchantAccountJournalReq);
			for(MerchantAccountJournal mer : merchantAccountJournals){
				AccountJournalQueryResponse accountJournalQueryResponse = new AccountJournalQueryResponse();
				BeanUtils.copyProperties(mer,accountJournalQueryResponse);
				list.add(accountJournalQueryResponse);
			}
			log.info("个人或者商户查询账户交易流水 返回参数。。。。。。{}", merchantAccountJournals);
		}
		//个人账户
		if(CustomerType.CUST_PERSONAL.getCode().equals(accountJournalQueryRequest.getCustomerType())){
			PersonalAccountJournal personalAccountJournalReq = new PersonalAccountJournal();
			personalAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
			personalAccountJournalReq.setPersonalId(customerBase.getPersonalId());
			personalAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
			List<PersonalAccountJournal> personalAccountJournals = personalAccountJournalMapper.select(personalAccountJournalReq);
			for(PersonalAccountJournal per : personalAccountJournals){
				AccountJournalQueryResponse accountJournalQueryResponse = new AccountJournalQueryResponse();
				BeanUtils.copyProperties(per,accountJournalQueryResponse);
				list.add(accountJournalQueryResponse);
			}
			log.info("个人或者商户查询账户交易流水 返回参数。。。。。。{}", personalAccountJournals);
		}
		return list;
	}


	/**
	 * 查询账户交易流水
	 * @time 创建时间:2018年9月27日下午3:57:14
	 * @param accountJournalQueryRequest
	 * @return
	 */
	public List<AccountJournalQueryResponse> accountDetailJournalQuery(AccountJournalQueryRequest accountJournalQueryRequest) {
		log.info("个人或者商户查询账户交易流水-请求参数：{}" + EntityUtils.beanToMap(accountJournalQueryRequest));
		List<AccountJournalQueryResponse> list = new ArrayList<AccountJournalQueryResponse>();
		//查询客户信息中的商户号和个人编号
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(accountJournalQueryRequest.getPlatformId());
		customerBase.setCustomerId(accountJournalQueryRequest.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		if(CustomerType.CUST_MERCHANT.getCode().equals(accountJournalQueryRequest.getCustomerType())){
			MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
			merchantAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
			merchantAccountJournalReq.setCustomerId(accountJournalQueryRequest.getCustomerId());
			merchantAccountJournalReq.setMerchantId(customerBase.getMerchantId());
			merchantAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
			List<MerchantAccountJournal> merchantAccountJournals = merchantAccountJournalMapper.select(merchantAccountJournalReq);
			for(MerchantAccountJournal mer : merchantAccountJournals){
				AccountJournalQueryResponse accountJournalQueryResponse = new AccountJournalQueryResponse();
				BeanUtils.copyProperties(mer,accountJournalQueryResponse);
				list.add(accountJournalQueryResponse);
			}
			log.info("个人或者商户查询账户交易流水 返回参数。。。。。。{}", merchantAccountJournals);
		}
		//个人账户
		if(CustomerType.CUST_PERSONAL.getCode().equals(accountJournalQueryRequest.getCustomerType())){
			PersonalAccountJournal personalAccountJournalReq = new PersonalAccountJournal();
			personalAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
			personalAccountJournalReq.setPersonalId(customerBase.getPersonalId());
			personalAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
			List<PersonalAccountJournal> personalAccountJournals = personalAccountJournalMapper.select(personalAccountJournalReq);
			for(PersonalAccountJournal per : personalAccountJournals){
				AccountJournalQueryResponse accountJournalQueryResponse = new AccountJournalQueryResponse();
				BeanUtils.copyProperties(per,accountJournalQueryResponse);
				list.add(accountJournalQueryResponse);
			}
			log.info("个人或者商户查询账户交易流水 返回参数。。。。。。{}", personalAccountJournals);
		}
		return list;
	}

	/**
	 * 查询订单状态
	 * 1、判断是余额账户 ， 交易类型为支付 ， 查询用户出账账务明细和商户入账账务明细
	 * 2、判断是余额账户 ， 交易类型为退款 ， 查询用户入账账务明细和商户出账账务明细
	 * 3、判断不是余额账户 ， 交易类型为支付 ， 查询商户入账账务明细
	 * 4、判断不是余额账户 ， 交易类型为退款 ， 查询商户出账账务明细
	 * 5、如果查询结果不为空返回200，如果查询结果为空返回错误码
	 * @author yyn
	 * @time 创建时间:2018年12月28日
	 * @param accountingOrderInfoReq
	 * @return
	 */
	public Map<String, Object> queryOrderInfo(AccountingOrderInfoReq accountingOrderInfoReq){
		
		log.info("查询订单状态请求参数：{}" + EntityUtils.beanToMap(accountingOrderInfoReq));
		Map<String, Object> baseResponse =  new HashMap<String, Object>();
		
		// 判断是否为余额账户
		if ("1".equals(accountingOrderInfoReq.getIsBalance())) {
			// 是余额账户
			if ("01".equals(accountingOrderInfoReq.getTradeType())) {
				// 支付
				PersonalAccountJournal personalAccountJournalReq = new PersonalAccountJournal();
				personalAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				personalAccountJournalReq.setInOutFlag(InOutFlag.out.getCode());// 出账
				personalAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				PersonalAccountJournal personalAccountJournalResp = personalAccountJournalMapper.selectOne(personalAccountJournalReq);
				
				MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
				merchantAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				merchantAccountJournalReq.setInOutFlag(InOutFlag.in.getCode());// 入账
				merchantAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				MerchantAccountJournal merchantAccountJournalRsp = merchantAccountJournalMapper.selectOne(merchantAccountJournalReq);
				
				if (personalAccountJournalReq != null && merchantAccountJournalRsp != null){
					baseResponse.put("code",BizResponseCode.OK.getCode());
					baseResponse.put("message",BizResponseCode.OK.getMessage());
				} else {
					baseResponse.put("code",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getCode());
					baseResponse.put("message",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getMessage());
				}
				
			}else{
				// 退款
				PersonalAccountJournal personalAccountJournalReq = new PersonalAccountJournal();
				personalAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				personalAccountJournalReq.setInOutFlag(InOutFlag.in.getCode());// 入账
				personalAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				PersonalAccountJournal personalAccountJournalRsp = personalAccountJournalMapper.selectOne(personalAccountJournalReq);
				
				MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
				merchantAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				merchantAccountJournalReq.setInOutFlag(InOutFlag.out.getCode());// 出账
				merchantAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				MerchantAccountJournal merchantAccountJournalRsp = merchantAccountJournalMapper.selectOne(merchantAccountJournalReq);
				
				if (personalAccountJournalReq != null && merchantAccountJournalRsp !=null) {
					baseResponse.put("code",BizResponseCode.OK.getCode());
					baseResponse.put("message",BizResponseCode.OK.getMessage());
				} else {
					baseResponse.put("code",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getCode());
					baseResponse.put("message",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getMessage());
				}
			}
		} else {
			// 不是余额账户
			if ("01".equals(accountingOrderInfoReq.getTradeType())) {
				// 支付
				MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
				merchantAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				merchantAccountJournalReq.setInOutFlag(InOutFlag.in.getCode());// 入账
				merchantAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				MerchantAccountJournal merchantAccountJournalRsp = merchantAccountJournalMapper.selectOne(merchantAccountJournalReq);
				
				if (merchantAccountJournalRsp != null) {
					baseResponse.put("code",BizResponseCode.OK.getCode());
					baseResponse.put("message",BizResponseCode.OK.getMessage());
				} else {
					baseResponse.put("code",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getCode());
					baseResponse.put("message",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getMessage());
				}
			}else{
				// 退款
				MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
				merchantAccountJournalReq.setOrderNo(accountingOrderInfoReq.getOrderNo());
				merchantAccountJournalReq.setInOutFlag(InOutFlag.out.getCode());// 出账
				merchantAccountJournalReq.setTradeType(accountingOrderInfoReq.getTradeType());
				MerchantAccountJournal merAccountingDetailResp = merchantAccountJournalMapper.selectOne(merchantAccountJournalReq);
				
				if (merAccountingDetailResp != null) {
					baseResponse.put("code",BizResponseCode.OK.getCode());
					baseResponse.put("message",BizResponseCode.OK.getMessage());
				} else {
					baseResponse.put("code",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getCode());
					baseResponse.put("message",BizResponseCode.ACCOUNT_TRANSACTION_STATUS_FAILED.getMessage());
				}
			}
		}
		
		log.info("查询订单状态参数：{}" + EntityUtils.beanToMap(baseResponse));
		return baseResponse;
	}
	
}
