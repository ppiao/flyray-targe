package me.flyray.crm.core.biz.customer;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.enums.MerchantType;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.util.SnowFlake;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.CustomerBaseAuth;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.mapper.CustomerBaseAuthMapper;
import me.flyray.crm.core.mapper.CustomerBaseMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;
import me.flyray.crm.facade.request.IntoAccountRequest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import me.flyray.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

/**
 * 客户授权信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerBaseAuthBiz extends BaseBiz<CustomerBaseAuthMapper, CustomerBaseAuth> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(CustomerBaseAuthBiz.class);

	@Autowired
	private CustomerBaseAuthMapper customerBaseAuthsMapper;
	@Autowired
	private PersonalBaseMapper personalBaseMapper;
	@Autowired
	private CustomerBaseMapper customerBaseMapper;
	@Autowired
	private PersonalDistributionRelationMapper relationMapper;
	@Autowired
	private CustomerIntoAccountBiz customerIntoAccountBiz;
	@Value("${wechat.appid}")
	private String wechatAppid;
	
	@Value("${wechat.secret}")
	private String wechatSecret;


	/**
	 * 微信小程序授权
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramAuth(Map<String, Object> request){
		logger.info("微信授权请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String code = (String) request.get("code");
		String isScanQr = (String) request.get("isScanQr");
		String inviteId = (String) request.get("inviteId");
		Map<String, Object> inviteMap = new HashMap<>();
		StringBuilder url = new StringBuilder();
		url.append("https://api.weixin.qq.com/sns/jscode2session?");
		url.append("appid="+wechatAppid);
		url.append("&secret="+wechatSecret);
		url.append("&js_code=").append(code);
		url.append("&grant_type=authorization_code");
		String data = null;
		try {
			logger.info("小程序获取openId第三方请求参数------{}", url.toString());
			data = wxSmallGetOpenId(url.toString());
		} catch (Exception e) {
			e.printStackTrace();
			response.put("code", "400101");
			response.put("message", "小程序授权失败");
			logger.info("微信授权响应。。。。。。{}", response);
			return response;
		}
		logger.info("小程序获取openId第三方返回参数------{}", JSONObject.fromObject(data));
		JSONObject json = JSONObject.fromObject(data);
		String openId = String.valueOf(json.get("openid"));

		String customerId = String.valueOf(SnowFlake.getId());
		String personalId = String.valueOf(SnowFlake.getId());
		String payPasswordStatus = "01";
		String phoneNum = "";
		
		CustomerBaseAuth customerBaseAuths = new CustomerBaseAuth();
		customerBaseAuths.setPlatformId(platformId);
		customerBaseAuths.setCustomerType("CUST02");//用户
		customerBaseAuths.setAuthMethod("weixin");//微信
		customerBaseAuths.setCredential(openId);
		CustomerBaseAuth selectAuthsInfo = customerBaseAuthsMapper.selectOne(customerBaseAuths);

		if(null == selectAuthsInfo){
			//为空则进行授权 
			customerBaseAuths.setCustomerId(customerId);
			customerBaseAuthsMapper.insert(customerBaseAuths);
			customerId = customerBaseAuths.getCustomerId();
			if ("01".equals(isScanQr)) {
				//执行邀请关系
				//相等说明自己邀请自己
				if(inviteId.equals(customerId)){
					inviteMap.put("code", "01");
					inviteMap.put("message", "不能邀请自己");
				}else {
					inviteMap = saveInvieteRelation(platformId, merchantId, customerId, inviteId);
				}
			}
		}else{
			//已经是会员或者已经被邀请过
			inviteMap.put("code", "01");
			inviteMap.put("message", "用户已经是会员不能被重复邀请");
			customerId = selectAuthsInfo.getCustomerId();
		}

		//判断用户基础信息表是否存在该用户
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(platformId);
		customerBase.setCustomerId(customerId);
		customerBase.setMerchantId(merchantId);
		CustomerBase selectCustomerBase = customerBaseMapper.selectOne(customerBase);
		if(null == selectCustomerBase){
			customerBase.setPersonalId(personalId);
			customerBase.setStatus("00");//正常
			customerBase.setRegisterTime(new Date());
			customerBase.setCreateTime(new Date());
			customerBase.setUpdateTime(new Date());
			customerBaseMapper.insert(customerBase);
			personalId = customerBase.getPersonalId();
		}else{
			personalId = selectCustomerBase.getPersonalId();
			if (!StringUtils.isEmpty(selectCustomerBase.getPayPassword())) {
				payPasswordStatus = "00";
			}
		}
		
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		personalBase.setPersonalType("00");//注册用户
		personalBase.setPersonalId(personalId);
		PersonalBase selectPersonalBase = personalBaseMapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			personalBaseMapper.insert(personalBase);
		} else {
			phoneNum = selectPersonalBase.getPhone();
		}

		response.put("openId", openId);
		response.put("customerId", customerId);
		response.put("personalId", personalId);
		response.put("inviteResult", inviteMap);
		response.put("payPassword", payPasswordStatus);
		response.put("phone", phoneNum);
		response.put("code", BizResponseCode.OK.getCode());
		response.put("message", BizResponseCode.OK.getMessage());
		logger.info("微信授权响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 小程序发送get请求 获取openId
	 * 
	 * @param url
	 *            路径
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static String wxSmallGetOpenId(String url) throws Exception {
		String strResult = null; // get请求返回结果
		CloseableHttpClient client = HttpClients.createDefault();

		HttpGet request = new HttpGet(url);// 发送get请求
		CloseableHttpResponse response = null;
		try {
			response = client.execute(request);
			// 请求发送成功，并得到响应
			logger.info("★★★★★★★★★★★★★★★★★★★★★★★★http response code:"+response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				strResult = EntityUtils.toString(response.getEntity()); // 读取服务器返回过来的json字符串数据
			} else {
				logger.info("get请求提交失败:" + url);
				return null;  
			}
		}catch (Exception e){
			logger.error("请求失败");
			throw new Exception("请求失败或者服务器错误");

		}
		return strResult;
	}

	/**
	 * 保存分享关系
	 * @param platformId
	 * @param customerId
	 * @param inviteId
	 * @return
	 */
	public Map<String, Object> saveInvieteRelation(String platformId, String merchantId, String customerId, String inviterId) {
		Map<String, Object> respMap = new HashMap<>();

		Map<String, Object> userMap = new HashMap<>();
		userMap.put("parentPerId", customerId);
		//将新用户与邀请人关联
		//判断邀请人属于哪级分销
		PersonalDistributionRelation distributionRelation = new PersonalDistributionRelation();
		distributionRelation.setPersonalId(inviterId);
		List<PersonalDistributionRelation> customerRelationses = relationMapper.select(distributionRelation);

		int sz = customerRelationses.size();
		Date invitedTime = new Date();
		logger.info("邀请关系层级数------{}",sz);
		/*if (sz == 0) {
			//说明邀请人是顶级分销不是被邀请过的人 受要人是一级分销
			PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
			invitedCustomer.setPlatformId(platformId);
			invitedCustomer.setPersonalId(customerId);
			invitedCustomer.setParentLevel(1);
			invitedCustomer.setParentPerId(inviterId);
			invitedCustomer.setCreateTime(invitedTime);
			relationMapper.insert(invitedCustomer);
		}else if(sz == 1) {
			//说明邀请人是一级分销 受邀人是二级分销需要写两条条记录
			PersonalDistributionRelation invitedCustomer1 = new PersonalDistributionRelation();
			invitedCustomer1.setPlatformId(platformId);
			invitedCustomer1.setPersonalId(customerId);
			invitedCustomer1.setParentLevel(1);
			invitedCustomer1.setParentPerId(inviterId);
			invitedCustomer1.setCreateTime(invitedTime);
			relationMapper.insert(invitedCustomer1);
			//受邀人
			String parentId =  customerRelationses.get(0).getParentPerId();
			PersonalDistributionRelation invitedCustomer2 = new PersonalDistributionRelation();
			invitedCustomer2.setPlatformId(platformId);
			invitedCustomer2.setPersonalId(customerId);
			invitedCustomer2.setParentLevel(2);
			invitedCustomer2.setParentPerId(parentId);
			invitedCustomer2.setCreateTime(invitedTime);
			relationMapper.insert(invitedCustomer2);
		}else if (sz == 2) {
			//说明邀请人是二级分销 受邀人是三级分销需要写三条条记录
			PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
			invitedCustomer.setPlatformId(platformId);
			invitedCustomer.setPersonalId(customerId);
			invitedCustomer.setParentLevel(1);
			invitedCustomer.setParentPerId(inviterId);
			invitedCustomer.setCreateTime(invitedTime);
			relationMapper.insert(invitedCustomer);
			//查询出邀请人的parentId
			for (PersonalDistributionRelation customerRelations : customerRelationses) {
				String parentId = customerRelations.getParentPerId();
				//判断邀请人的上级位于三级分销中第几级
				PersonalDistributionRelation ic = new PersonalDistributionRelation();
				if (1 == customerRelations.getParentLevel()) {
					ic.setParentLevel(2);
				}else if (2 == customerRelations.getParentLevel()) {
					ic.setParentLevel(3);
				}
				ic.setPlatformId(platformId);
				ic.setPersonalId(customerId);
				ic.setParentPerId(parentId);
				ic.setCreateTime(invitedTime);
				relationMapper.insert(ic);
			}
		}else if (sz == 3) {
			//说明邀请人是三级分销 受邀人是邀请人的一级分销需要写一条记录
			PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
			invitedCustomer.setPlatformId(platformId);
			invitedCustomer.setPersonalId(customerId);
			invitedCustomer.setParentLevel(1);
			invitedCustomer.setParentPerId(inviterId);
			invitedCustomer.setCreateTime(invitedTime);
			relationMapper.insert(invitedCustomer);
		}*/
		//增加原力值
		Map<String, Object> accountResp = new HashMap<>();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(platformId);
		customerBase.setCustomerId(inviterId);
		customerBase.setMerchantId(merchantId);
		CustomerBase selectCustomerBase = customerBaseMapper.selectOne(customerBase);
		String personalId = selectCustomerBase.getPersonalId();
		IntoAccountRequest intoAccountRequest = new IntoAccountRequest();
		intoAccountRequest.setPlatformId(platformId);
		intoAccountRequest.setMerchantId(merchantId);
		intoAccountRequest.setOrderNo(String.valueOf(SnowFlake.getId()));
		intoAccountRequest.setIntoAccAmt("2");
		intoAccountRequest.setMerchantType(MerchantType.MER_MERCHANT.getCode());
		intoAccountRequest.setCustomerType("02");
		intoAccountRequest.setAccountType("ACC012");
		intoAccountRequest.setTradeType("06");
		intoAccountRequest.setTxFee("0");
		intoAccountRequest.setPersonalId(personalId);
		customerIntoAccountBiz.intoAccount(intoAccountRequest);
		log.info("扫码分享，领取原力值。。。结果 "+accountResp);
		respMap.put("code", "00");
		respMap.put("message", "邀请成功");
		return respMap;
	}


	/**
	 * 生成二维码流
	 * @param urls
	 * @param map
	 * @return
	 */
	public byte[]  doImgPost(String str) {

		byte[] result = null;
		//获取access_token
		StringBuilder url = new StringBuilder();
		url.append("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential");
		url.append("&appid="+wechatAppid);
		url.append("&secret="+wechatSecret);
		String data = null;
		try {
			logger.info("小程序获取access_token第三方请求参数------{}", url.toString());
			data = wxSmallGetOpenId(url.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("小程序获取access_token第三方返回参数------{}", JSONObject.fromObject(data));
		JSONObject json = JSONObject.fromObject(data);
		String access_token = String.valueOf(json.get("access_token"));

		//使用HTTPPost方法访问获取二维码链接url
//		String urlStr = "https://api.weixin.qq.com/wxa/getwxacode?access_token="+access_token;
		String urlStr = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+access_token;
		HttpPost httpPost = new HttpPost(urlStr);
		//创建http连接客户端
		CloseableHttpClient client = HttpClients.createDefault();
		//设置头响应类型
		httpPost.addHeader("Content-Type", "application/json");

		Map<String, Object> paraMap = new HashMap<>();
		//二维码携带参数 不超过32位
		paraMap.put("scene", str);
		//二维码跳转页面
		paraMap.put("path", "pages/index/index");
		paraMap.put("width", 430);
		paraMap.put("auto_color", false);
		paraMap.put("line_color", "{\"r\":\"0\",\"g\":\"0\",\"b\":\"0\"}");
		try {
			// 设置请求的参数
			JSONObject postData = new JSONObject();
			for (Map.Entry<String, Object> entry : paraMap.entrySet()) {
				postData.put(entry.getKey(), entry.getValue());
			}
			httpPost.setEntity(new StringEntity(postData.toString(), "UTF-8"));
			logger.info("微信获取微信二维码post数据 " + postData.toString());
			//返回的post请求结果
			HttpResponse response = client.execute(httpPost);
			HttpEntity entity = response.getEntity();
			result = EntityUtils.toByteArray(entity);
		} catch (ConnectionPoolTimeoutException e) {
			logger.error("http get throw ConnectionPoolTimeoutException(wait time out)", e);
		} catch (ConnectTimeoutException e) {
			logger.error("http get throw ConnectTimeoutException", e);
		} catch (SocketTimeoutException e) {
			logger.error("http get throw SocketTimeoutException", e);
		} catch (Exception e) {
			logger.error("http get throw Exception", e);
		} finally {
			httpPost.releaseConnection();
		}
		//最后转成2进制图片流
		return result;
	}
	

}