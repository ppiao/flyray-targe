package me.flyray.crm.core.biz.personal;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.vo.PersonalLevelRequest;
import me.flyray.common.vo.QueryPersonalBaseExtRequest;
import me.flyray.common.vo.QueryPersonalInfoExtResponse;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;
import me.flyray.crm.core.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import org.springframework.util.CollectionUtils;
import sun.font.TrueTypeFont;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 个人基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@Service
public class PersonalBaseExtBiz {

    @Resource
    private PersonalBaseExtMapper personalBaseExtMapper;
    @Resource
    private PersonalBaseMapper personalBaseMapper;
    @Resource
    private PersonalDistributionRelationMapper personalDistributionRelationMapper;

    public List<QueryPersonalInfoExtResponse> queryWithPointsList(QueryPersonalBaseExtRequest request, PageResult pageResult) {
        Integer pageSize = request.getPageSize();
        Integer currentPage = request.getCurrentPage();
        if(pageSize == null || pageSize.intValue() == 0){
            pageSize = 10;
        }
        if(currentPage == null || currentPage.intValue() == 0){
            currentPage = 1;
        }
        Integer startRow = (currentPage -1) * pageSize;
        if("1".equals(request.getUserType())){
            request.setOwnerId(request.getCustomerId());
        } else{
            request.setOwnerId(request.getUserId());
        }
        Integer count  = personalBaseExtMapper.getPageCount(request.getPersonalId(), request.getRealName(), request.getMobile(),
                request.getAuthenticationStatus(), request.getOwnerId(), request.getPersonalLevel());
        List<PersonalBase> personalBaseExtList = personalBaseExtMapper.getPageInfos(request.getPersonalId(), request.getRealName(), request.getMobile(),
                request.getAuthenticationStatus(), request.getOwnerId(), request.getPersonalLevel(), startRow, pageSize);
        List<QueryPersonalInfoExtResponse> queryPersonalInfoExtResponseList = new ArrayList<QueryPersonalInfoExtResponse>();
        if(!CollectionUtils.isEmpty(personalBaseExtList)){
            for (PersonalBase personalBase :personalBaseExtList) {
                QueryPersonalInfoExtResponse queryPersonalInfoExtResponse = buildQueryPersonalInfoExtResponse(personalBase);
                queryPersonalInfoExtResponseList.add(queryPersonalInfoExtResponse);
            }
        }
        Integer pageCount = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
        pageResult.setCurrentPage(currentPage);
        pageResult.setPageCount(pageCount);
        pageResult.setRowCount(count);
        pageResult.setPageSize(pageSize);
        return queryPersonalInfoExtResponseList;
    }


    public BaseApiResponse updatePersonalLevels(PersonalLevelRequest request) {
        for(String personalId : request.getPersonalIds()) {
            personalBaseExtMapper.updateLevelByPersonalId(personalId, request.getPersonalLevel());
        }
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }

    private QueryPersonalInfoExtResponse buildQueryPersonalInfoExtResponse(PersonalBase personalBase) {
        QueryPersonalInfoExtResponse queryInfo = new QueryPersonalInfoExtResponse();
        queryInfo.setId(personalBase.getId());
        queryInfo.setPhone(personalBase.getPhone());
        queryInfo.setRealName(personalBase.getRealName());
        queryInfo.setOwnerId(personalBase.getOwnerId());
        queryInfo.setOwnerName(personalBase.getOwnerName());
        queryInfo.setUpdateTime(personalBase.getUpdateTime());
        queryInfo.setPersonalLevel(personalBase.getParentLevel());
        queryInfo.setSex("男");
        if("2".equals(personalBase.getSex())){
            queryInfo.setSex("女");
        }
        queryInfo.setAuthenticationStatus("未激活");
        if("02".equals(personalBase.getAuthenticationStatus())){
            queryInfo.setAuthenticationStatus("已激活");
        }
        //TODO 积分功能未实现
        queryInfo.setPointsBalance("");
        PersonalDistributionRelation personalDistributionRelation = personalDistributionRelationMapper.queryByPersonalId(queryInfo.getPersonalId());
        if(personalDistributionRelation != null){
            queryInfo.setReferrerId(personalDistributionRelation.getParentId());
            queryInfo.setReferrerName(personalDistributionRelation.getParentName());
        }
        return queryInfo;
    }

}