package me.flyray.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_coin_config")
public class PlatformCoinConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //平台编号
    @Id
    private Long platformId;
	
	    //1：顶级平台 2：普通平台
    @Column(name = "platform_type")
    private String platformType;
	
	    //平台代币名称
    @Column(name = "name")
    private String name;
	
	    //代币的简称
    @Column(name = "symbol")
    private String symbol;
	
	    //平台币总量
    @Column(name = "sum")
    private Long sum;
	

	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：1：顶级平台 2：普通平台
	 */
	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}
	/**
	 * 获取：1：顶级平台 2：普通平台
	 */
	public String getPlatformType() {
		return platformType;
	}
	/**
	 * 设置：平台代币名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：平台代币名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：代币的简称
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	/**
	 * 获取：代币的简称
	 */
	public String getSymbol() {
		return symbol;
	}
	/**
	 * 设置：平台币总量
	 */
	public void setSum(Long sum) {
		this.sum = sum;
	}
	/**
	 * 获取：平台币总量
	 */
	public Long getSum() {
		return sum;
	}
}
