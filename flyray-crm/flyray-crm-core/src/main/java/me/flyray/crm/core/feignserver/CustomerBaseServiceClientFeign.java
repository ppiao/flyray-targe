package me.flyray.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.util.SHA256Utils;
import io.swagger.annotations.ApiOperation;
import me.flyray.crm.core.biz.customer.CustomerAccountQueryBiz;
import me.flyray.crm.core.mapper.PlatformSafetyConfigMapper;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.PlatformSafetyConfig;
import me.flyray.crm.facade.request.LoginRequest;
import me.flyray.crm.facade.request.RegisterRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerBaseBiz;

import javax.validation.Valid;

@Controller
@RequestMapping("feign/customer")
public class CustomerBaseServiceClientFeign {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerBaseServiceClientFeign.class);
	
	@Autowired
	private PlatformSafetyConfigMapper platformSafetyConfigMapper;
	@Autowired
	private CustomerBaseBiz customerBaseBiz;
	@Autowired
	private CustomerAccountQueryBiz commonAccountQueryBiz;

	/**
	 * 用户注册
	 * @time 创建时间:2019年1月7日
	 * @return
	 */
	@ApiOperation("用户注册")
	@RequestMapping(value = "/register",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<CustomerBase> register(@RequestBody @Valid RegisterRequest registerRequest) {
		CustomerBase customerBase = customerBaseBiz.register(registerRequest);
		return BaseApiResponse.newSuccess(customerBase);
	}

	/**
	 * 用户登录
	 * @time 创建时间:2019年1月7日
	 * @param loginRequest
	 * @return
	 */
	@ApiOperation("用户登录")
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<CustomerBase> login(@RequestBody @Valid LoginRequest loginRequest) {
		CustomerBase customerBase = customerBaseBiz.login(loginRequest);
		return BaseApiResponse.newSuccess(customerBase);
	}
	
	/**
	 * 校验交易密码
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/verifyPayPassword",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	
		String platformId = (String) param.get("platformId");
		String customerId = (String) param.get("customerId");
		String payPassword = (String) param.get("payPassword");
		
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(platformId);
		customerBaseReq.setCustomerId(customerId);
		CustomerBase customerBase = customerBaseBiz.selectOne(customerBaseReq);
		if (null == customerBase) {
			result.put("code", BizResponseCode.CUST_NOTEXIST.getCode());
			result.put("msg", BizResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【校验交易密码】   响应参数：{}", result);
			return result;
		}
		if (StringUtils.isEmpty(customerBase.getPayPassword())) {
			result.put("code", BizResponseCode.PWD_ERROR.getCode());
			result.put("msg", BizResponseCode.PWD_ERROR.getMessage());
			log.info("【校验交易密码】   响应参数：{}", result);
			return result;
		}
		
		PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
		platformSafetyConfigReq.setPlatformId(platformId);
		PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfigReq);
		if (platformSafetyConfig != null) {
			String appKey = platformSafetyConfig.getAppKey();
			if (!StringUtils.isEmpty(appKey)) {
				String newPwd = SHA256Utils.SHA256(SHA256Utils.SHA256(payPassword)+appKey);
				if (newPwd.equals(customerBase.getPayPassword())) {
					result.put("code", BizResponseCode.OK.getCode());
					result.put("msg", BizResponseCode.OK.getMessage());
				}else {
					result.put("code", BizResponseCode.PAY_PWD_ERROR.getCode());
					result.put("msg", BizResponseCode.PAY_PWD_ERROR.getMessage());
				}
			}else {
				result.put("code", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
				result.put("msg", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
				return result;
			}
		}else {
			//UNIONID_IS_NULL
			result.put("code", BizResponseCode.UNIONID_IS_NULL.getCode());
			result.put("msg", BizResponseCode.UNIONID_IS_NULL.getMessage());
			return result;
		}
		log.info("【校验交易密码】   响应参数：{}", result);
    	return result;
    }

}
