package me.flyray.crm.core.feignserver;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.entity.FreezeJournal;
import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.facade.feignclient.modules.customer.CustomerFreezeOrUnfreezeServiceClient;
import me.flyray.crm.facade.request.FreezeRequest;
import me.flyray.crm.facade.request.UnfreezeRequest;
import me.flyray.crm.facade.response.FreezeJournalResponse;
import me.flyray.crm.facade.response.UnfreezeJournalResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerFreezeOrUnfreezeBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 冻结、解冻相关的接口
 * */
@Api(tags="冻结、解冻相关的接口")
@Controller
@RequestMapping("feign/customer")
public class CustomerFreezeOrUnfreezeServiceClientFeign implements CustomerFreezeOrUnfreezeServiceClient {
	
	@Autowired
	private CustomerFreezeOrUnfreezeBiz commonFreezefnFreezeBiz;
	
	
	/***
	 * 个人或者商户冻结
	 * */
	@Override
	@ResponseBody
    public BaseApiResponse<FreezeJournalResponse> freeze(@RequestBody @Valid FreezeRequest freezeRequest){
		FreezeJournal freezeJournal = commonFreezefnFreezeBiz.freeze(freezeRequest);
		FreezeJournalResponse freezeJournalResponse = new FreezeJournalResponse();
		BeanUtils.copyProperties(freezeJournal, freezeJournalResponse);
		return BaseApiResponse.newSuccess(freezeJournalResponse);
	}
	
	/**
	 * 个人或者商户解冻
	 * */
	@Override
	@ResponseBody
    public BaseApiResponse<UnfreezeJournalResponse> unfreeze(@RequestBody @Valid UnfreezeRequest unFreezeRequest){
		UnfreezeJournal unfreezeJournal =  commonFreezefnFreezeBiz.unfreeze(unFreezeRequest);
		UnfreezeJournalResponse unfreezeJournalResponse = new UnfreezeJournalResponse();
		BeanUtils.copyProperties(unfreezeJournal, unfreezeJournalResponse);
		return BaseApiResponse.newSuccess(unfreezeJournalResponse);
    }

}
