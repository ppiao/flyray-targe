package me.flyray.crm.core.feignserver;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.flyray.common.enums.CustomerType;
import me.flyray.common.exception.BusinessException;
import me.flyray.common.msg.AccountType;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.util.ResponseHelper;
import me.flyray.crm.core.biz.customer.CustomerBaseAuthBiz;
import me.flyray.crm.core.biz.customer.CustomerBaseBiz;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;
import me.flyray.crm.core.biz.personal.PersonalBaseBiz;
import me.flyray.crm.core.biz.personal.PersonalDistributionRelationBiz;
import me.flyray.crm.core.entity.*;
import me.flyray.crm.facade.feignclient.modules.personal.PersonalBaseServiceClient;
import me.flyray.crm.facade.request.DistributionInviteRequest;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import me.flyray.crm.facade.request.RegisterRequest;
import me.flyray.crm.facade.request.personal.Personal3rdAuthRegisterRequest;
import me.flyray.crm.facade.request.personal.PersonalAuthStatusRequest;
import me.flyray.crm.facade.response.personal.PersonalInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 20:53 2019/2/15
 * @Description: 个人客户
 */

@Api(tags="个人客户")
@Controller
@Slf4j
public class PersonalBaseServiceClientFeign implements PersonalBaseServiceClient {

    @Autowired
    private PersonalBaseBiz personalBaseBiz;
    @Autowired
    private CustomerBaseBiz customerBaseBiz;
    @Autowired
    private CustomerBaseAuthBiz customerBaseAuthBiz;
    @Autowired
    private PersonalAccountBiz personalAccountBiz;
    @Autowired
    private PersonalDistributionRelationBiz personalDistributionRelationBiz;

    /**
     * 个人客户查询
     * @author centerroot
     * @time 创建时间:2018年9月26日下午4:31:39
     * @param personalBaseRequest
     * @return
     */
    @ApiOperation("个人客户查询")
    @RequestMapping(value = "feign/personal/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    @Override
    public BaseApiResponse<PersonalInfo> queryInfo(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
        Map<String, Object> respMap = new HashMap<>();
        PersonalBase personalBaseReq = new PersonalBase();
        BeanUtils.copyProperties(personalBaseRequest, personalBaseReq);
        PersonalBase personalBase = personalBaseBiz.selectOne(personalBaseReq);
        PersonalInfo personalInfo = new PersonalInfo();
        if (null == personalBase) {
            throw new BusinessException(BizResponseCode.PER_NOTEXIST);
        }else{
            BeanUtils.copyProperties(personalBase, personalInfo);
        }
        return BaseApiResponse.newSuccess(personalInfo);
    }

    /**
     * @param request
     * @return
     * 分销邀请好友
     * 三级分销一个用户上面最多出现不同级别的三个用户
     */
    @Override
    @ResponseBody
    public BaseApiResponse invite(@RequestBody DistributionInviteRequest request){
        //将新用户与邀请人关联
        //判断邀请人属于哪级分销
        String inviterPersonalId = request.getInviterPersonalId();
        String inviteePersonalId = request.getInviteePersonalId();
        PersonalBase personalBaseReq = new PersonalBase();
        personalBaseReq.setPersonalId(inviterPersonalId);
        PersonalBase personalBase = personalBaseBiz.selectOne(personalBaseReq);
        List<PersonalDistributionRelation> customerRelationses = personalDistributionRelationBiz.queryByPersonalId(inviterPersonalId);
        int sz = customerRelationses.size();
        Date invitedTime = new Date();
        log.info("邀请关系层级数------{}",sz);
        if (sz == 0) {
            //说明邀请人是顶级分销没有被邀请过的人 受邀人是一级分销
            PersonalDistributionRelation inviteeCustomer = new PersonalDistributionRelation();
            inviteeCustomer.setPersonalId(inviteePersonalId);
            inviteeCustomer.setFxLevel("1");
            inviteeCustomer.setParentId(inviterPersonalId);
            inviteeCustomer.setParentName(personalBase.getNickName());
            inviteeCustomer.setCreateTime(invitedTime);
            personalDistributionRelationBiz.insert(inviteeCustomer);
        }else if(sz == 1) {
            //说明邀请人是第一级分销 受邀人是二级分销需要写两条条记录
            PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
            invitedCustomer.setPersonalId(inviteePersonalId);
            invitedCustomer.setFxLevel("1");
            invitedCustomer.setParentId(inviterPersonalId);
            invitedCustomer.setParentName(personalBase.getNickName());
            invitedCustomer.setCreateTime(invitedTime);
            //受邀人
            PersonalDistributionRelation personalDistributionRelation = customerRelationses.get(0);
            PersonalDistributionRelation ic = new PersonalDistributionRelation();
            ic.setPersonalId(inviteePersonalId);
            ic.setFxLevel("2");
            ic.setParentId(personalDistributionRelation.getParentId());
            ic.setParentName(personalBase.getNickName());
            ic.setCreateTime(invitedTime);
            personalDistributionRelationBiz.insert(ic);
        }else if (sz == 2) {
            //说明邀请人是第二级分销 受邀人是三级分销需要写三条条记录
            PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
            invitedCustomer.setPersonalId(inviteePersonalId);
            invitedCustomer.setFxLevel("1");
            invitedCustomer.setParentId(inviterPersonalId);
            invitedCustomer.setParentName(personalBase.getNickName());
            invitedCustomer.setCreateTime(invitedTime);
            personalDistributionRelationBiz.insert(invitedCustomer);
            //查询出邀请人的parentId
            for (PersonalDistributionRelation customerRelations : customerRelationses) {
                String parentId = customerRelations.getParentId();
                //判断邀请人的上级位于三级分销中第几级
                PersonalDistributionRelation ic = new PersonalDistributionRelation();
                ic.setPersonalId("");
                if ("1" == customerRelations.getFxLevel()) {
                    ic.setFxLevel("2");
                }else if ("2" == customerRelations.getFxLevel()) {
                    ic.setFxLevel("3");
                }
                ic.setParentId(parentId);
                personalDistributionRelationBiz.insert(ic);
            }
        }else if (sz == 3) {
            //说明邀请人是第三级分销 受邀人是邀请人的一级分销需要写一条记录
            PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
            invitedCustomer.setPersonalId(inviteePersonalId);
            invitedCustomer.setParentId(inviterPersonalId);
            invitedCustomer.setParentName(personalBase.getNickName());
            invitedCustomer.setCreateTime(invitedTime);
            invitedCustomer.setFxLevel("1");
            personalDistributionRelationBiz.insert(invitedCustomer);
        }else if(sz > 3){
            PersonalDistributionRelation invitedCustomer = new PersonalDistributionRelation();
            invitedCustomer.setPersonalId(inviteePersonalId);
            invitedCustomer.setParentId(inviterPersonalId);
            invitedCustomer.setParentName(personalBase.getNickName());
            invitedCustomer.setCreateTime(invitedTime);
            invitedCustomer.setFxLevel("9999");
            personalDistributionRelationBiz.insert(invitedCustomer);
        }
        return BaseApiResponse.newSuccess();
    }

    /**
     * @param request
     * @return
     * 用户授权状态查询
     * 1:未激活 2:已激活 3:客户信息为补全
     * 查询customer_base_auth表信息查看用户是否授权
     */
    @Override
    @ResponseBody
    public BaseApiResponse<PersonalInfo> queryAuthStatus(@RequestBody @Valid PersonalAuthStatusRequest request){
        CustomerBaseAuth customerBaseAuth = new CustomerBaseAuth();
        customerBaseAuth.setAuthMethod(request.getThirdCode());
        customerBaseAuth.setCredential(request.getXId());
        customerBaseAuth = customerBaseAuthBiz.selectOne(customerBaseAuth);
        PersonalInfo personalInfo = new PersonalInfo();
        if (customerBaseAuth == null){
            personalInfo.setStatus("1");
            return BaseApiResponse.newSuccess(personalInfo);
        }
        //查询用户信息是否补全
        PersonalBase personalBase = new PersonalBase();
        personalBase.setPlatformId(customerBaseAuth.getPlatformId());
        personalBase.setPersonalId(customerBaseAuth.getCustomerId());
        personalBase = personalBaseBiz.selectOne(personalBase);
        if (personalBase == null ){
            personalInfo.setStatus("1");
            return BaseApiResponse.newSuccess(personalInfo);
        }
        if (!StringUtils.isEmpty(personalBase.getRealName())){
            personalInfo.setStatus("3");
        }else {
            personalInfo.setStatus("2");
        }
        //查询用户的余额信息
        PersonalAccount personalAccount = new PersonalAccount();
        personalAccount.setPersonalId(personalBase.getPersonalId());
        personalAccount.setAccountType(AccountType.ACC_INTEGRAL.getCode());
        personalAccount = personalAccountBiz.selectOne(personalAccount);
        personalInfo.setBalance(String.valueOf(personalAccount.getAccountBalance()));
        return BaseApiResponse.newSuccess(personalInfo);
    }

    /**
     * @param request
     * @return
     * 第三方授权授权,及基础信息授权注册
     * 将用户信息写入customer_base和customer_base_auth及personal_base
     */
    @Override
    @ResponseBody
    public BaseApiResponse personal3rdAuthRegister(@RequestBody @Valid Personal3rdAuthRegisterRequest request){
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
        registerRequest.setPlatformId(request.getPlatformId());
        registerRequest.setPhone(request.getPhone());
        CustomerBase customerBase = customerBaseBiz.register(registerRequest);
        CustomerBaseAuth customerBaseAuth = new CustomerBaseAuth();
        customerBaseAuth.setAuthMethod(request.getThirdCode());
        customerBaseAuth.setCredential(request.getXId());
        customerBaseAuth.setIdentifier(request.getNickname());
        customerBaseAuth.setCustomerId(customerBase.getPersonalId());
        customerBaseAuth.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
        customerBaseAuthBiz.insert(customerBaseAuth);
        return BaseApiResponse.newSuccess();
    }

    /**
     * @param request
     * @return
     * 更新用户信息
     */
    @Override
    @ResponseBody
    public BaseApiResponse updatePersonalInfo(@RequestBody PersonalBaseRequest request){
        personalBaseBiz.updateObj(request);
        return BaseApiResponse.newSuccess();
    }

}
