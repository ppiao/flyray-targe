package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.CustomerBaseAuth;
import tk.mybatis.mapper.common.Mapper;

/**
 * 客户授权信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerBaseAuthMapper extends Mapper<CustomerBaseAuth> {
	
}
