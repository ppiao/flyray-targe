package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.CustomerBase;
import tk.mybatis.mapper.common.Mapper;

/**
 * 客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerBaseMapper extends Mapper<CustomerBase> {
	
}
