package me.flyray.crm.core.mapper;

import java.util.List;
import java.util.Map;

import me.flyray.crm.core.entity.PersonalDistributionRelation;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人分销关系
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalDistributionRelationMapper extends Mapper<PersonalDistributionRelation> {

	PersonalDistributionRelation queryByPersonalId(String personalId);

}
