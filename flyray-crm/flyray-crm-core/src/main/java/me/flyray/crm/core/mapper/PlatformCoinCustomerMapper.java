package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PlatformCoinCustomer;

import tk.mybatis.mapper.common.Mapper;

/**
 * 平台唯一用户表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformCoinCustomerMapper extends Mapper<PlatformCoinCustomer> {
	
}
