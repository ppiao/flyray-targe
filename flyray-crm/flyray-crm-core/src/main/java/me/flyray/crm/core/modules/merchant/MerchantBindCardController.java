package me.flyray.crm.core.modules.merchant;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.merchant.MerchantBindCardBiz;
import lombok.extern.slf4j.Slf4j;
import me.flyray.crm.core.entity.MerchantBindCard;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("merchantBindCard")
public class MerchantBindCardController extends BaseController<MerchantBindCardBiz, MerchantBindCard> {

}