package me.flyray.crm.core.modules.personal;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.vo.PersonalLevelRequest;
import me.flyray.common.vo.QueryPersonalBaseExtRequest;
import me.flyray.common.vo.QueryPersonalInfoExtResponse;
import me.flyray.crm.core.biz.personal.PersonalBaseBiz;
import me.flyray.crm.core.biz.personal.PersonalBaseExtBiz;
import me.flyray.crm.core.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("personalBaseExt/ext")
public class PersonalBaseExtController {

    @Autowired
    private PersonalBaseExtBiz personalBaseExtBiz;

    /**
     * 查询个人及积分基础信息
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryWithPointsList", method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<QueryPersonalInfoExtResponse> queryWithPointsList(@RequestBody @Valid QueryPersonalBaseExtRequest request){

        try {
            PageResult pageResult = new PageResult();
            request.setUserId(BaseContextHandler.getXId());
            request.setUserType(BaseContextHandler.getUserType());
            List<QueryPersonalInfoExtResponse> result = personalBaseExtBiz.queryWithPointsList(request, pageResult);
            return new TableResultResponse<QueryPersonalInfoExtResponse>(pageResult.getRowCount(), result);
        } catch (Exception e) {
            log.error("PersonalBaseExtController queryWithPointsList, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            TableResultResponse<QueryPersonalInfoExtResponse> response = new TableResultResponse<QueryPersonalInfoExtResponse>();
            response.setCode(ResponseCode.SYSTEM_ERROR.getCode());
            response.setMessage(ResponseCode.SYSTEM_ERROR.getMessage());
            return response;
        }
    }


    /**
     * 查询个人及积分基础信息
     * @author centerroot
     * @time
     * @param request
     * @return
     */
    @RequestMapping(value = "/updatePersonalLevels", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse updatePersonalLevels(@RequestBody @Valid PersonalLevelRequest request){

        try {
            return  personalBaseExtBiz.updatePersonalLevels(request);
        } catch (Exception e) {
            log.error("PersonalBaseExtController updatePersonalLevels, request={}, Exception = {}", EntityUtils.beanToMap(request), e);
            return  BaseApiResponse.newFailure( ResponseCode.SYSTEM_ERROR);
        }
    }
}