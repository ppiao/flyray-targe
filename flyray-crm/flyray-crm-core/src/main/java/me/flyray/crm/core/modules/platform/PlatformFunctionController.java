package me.flyray.crm.core.modules.platform;

import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PlatformFunction;
import me.flyray.crm.core.biz.platform.PlatformFunctionBiz;
import me.flyray.crm.facade.request.PlatformFunctionRequestParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.common.util.ResponseHelper;


/**
 * 平台功能
 * @author Administrator
 *
 */
@Controller
@RequestMapping("baseFunction")
public class PlatformFunctionController extends BaseController<PlatformFunctionBiz, PlatformFunction> {
	
	private static final Logger logger = LoggerFactory.getLogger(PlatformFunctionController.class);
	
	/**
	 * 查询平台功能列表
	 * @param param
	 * @return
	 */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
	public TableResultResponse<PlatformFunction> query(@RequestParam Map<String, Object> param) {
    	logger.info("查询平台功能列表。。。{}"+param);
		return baseBiz.queryList(param);
	}
    
    
    @ResponseBody
    @RequestMapping(value = "/allList", method = RequestMethod.GET)
	public Map<String, Object> queryWithLevel(@RequestParam Map<String, Object> param) {
    	logger.info("查询平台功能列表。。。{}"+param);
    	Map<String, Object> map = baseBiz.queryWithLevel(param);
    	if (map != null) {
    		return ResponseHelper.success(map, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(map, null, BizResponseCode.FUNCTION_NO_EXIST.getCode(), BizResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
    
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody PlatformFunctionRequestParam param) {
    	logger.info("添加平台功能。。。{}"+param);
    	PlatformFunction func = baseBiz.addFunction(param);
    	if (func == null) {
    		return ResponseHelper.success(null, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, BizResponseCode.FUNCTION_EXIST.getCode(), BizResponseCode.FUNCTION_EXIST.getMessage());
		}
	}
    
    
    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody PlatformFunctionRequestParam param) {
    	logger.info("修改平台功能。。。{}"+param);
		baseBiz.updateFunction(param);
		return ResponseHelper.success(null, null, "200", "修改成功");
	}
    
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody PlatformFunctionRequestParam param) {
    	logger.info("删除平台功能。。。{}"+param);
		baseBiz.deleteFunction(param);
		return ResponseHelper.success(null, null, "200", "删除成功");
	}

}