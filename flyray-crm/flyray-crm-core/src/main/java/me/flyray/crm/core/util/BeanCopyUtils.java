package me.flyray.crm.core.util;

import org.springframework.cglib.beans.BeanCopier;

/**
 * @Auther: luya
 * @Date: 2018/11/5/005 14:44
 * @Description:
 */
public class BeanCopyUtils {
    
    /**
     *
     * Description: 
     *
     * @param:
     * @return: 
     * @date: 2018/11/5/005 14:46
     */
    public static void copy(Object source, Class sourceClass, Object target, Class targetClass) {

        if(source == null || target == null) {
            return;
        }

        BeanCopier copier = BeanCopier.create(sourceClass, targetClass, false);

        copier.copy(source, target, null);

    }
    
}
