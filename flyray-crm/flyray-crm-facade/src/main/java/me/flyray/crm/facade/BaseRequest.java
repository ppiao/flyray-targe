package me.flyray.crm.facade;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求参数基类
 * */
@Data
public class BaseRequest implements Serializable {

	/**
	 * 接口版本号
	 */
	private String version;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 请求流水号
	 */
	private String reqJrnNo;
	/**
	 * 请求时间戳
	 */
	private String reqDatetime;
	/**
	 * 请求方法名
	 */
	private String txCode;

}
