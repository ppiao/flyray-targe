package me.flyray.crm.facade.feignclient.modules.admin;

import java.util.Map;

import me.flyray.common.vo.admin.AddPlatformOrMerchantRequest;
import me.flyray.common.vo.admin.AdminUserRequest;
import me.flyray.crm.facade.request.PlatformBaseAddRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "flyray-admin")
public interface AdminUserServiceClient {

	@RequestMapping(value = "feign/role/add",method = RequestMethod.POST)
    public Map<String, Object> addRole(@RequestBody Map<String, Object> param);
	
	@RequestMapping(value = "feign/dept/add",method = RequestMethod.POST)
    public Map<String, Object> addDept(@RequestBody Map<String, Object> param);
	
	@RequestMapping(value = "feign/user/add",method = RequestMethod.POST)
    public Map<String, Object> addUser(@RequestBody AdminUserRequest param);
	
	@RequestMapping(value = "feign/user/queryByMobile",method = RequestMethod.POST)
    public Map<String, Object> queryByMobile(@RequestBody Map<String, Object> param);
	
	@RequestMapping(value = "feign/authority/platform",method = RequestMethod.POST)
    public Map<String, Object> platformAuthority(@RequestBody Map<String, Object> param);

	@RequestMapping(value = "feign/dept/selectByPlatformId",method = RequestMethod.POST)
    public Map<String, Object> selectByPlatformId(@RequestBody Map<String, Object> param);

	@RequestMapping(value = "feign/dept/update",method = RequestMethod.POST)
    public Map<String, Object> updateDept(@RequestBody Map<String, Object> param);
	
	@RequestMapping(value = "feign/dict/selectByType",method = RequestMethod.POST)
    public Map<String, Object> selectByType(@RequestBody Map<String, Object> param);
	
	@RequestMapping(value = "feign/common/addPlatformOrMerchant",method = RequestMethod.POST)
    public Map<String, Object> addPlatformOrMerchant(@RequestBody AddPlatformOrMerchantRequest param);
	
	@RequestMapping(value = "feign/common/deletePlatformOrMerchant",method = RequestMethod.POST)
    public Map<String, Object> deletePlatformOrMerchant(@RequestBody Map<String, Object> param);
	

}
