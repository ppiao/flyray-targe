package me.flyray.crm.facade.feignclient.modules.personal;

import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.DistributionInviteRequest;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import me.flyray.crm.facade.request.personal.Personal3rdAuthRegisterRequest;
import me.flyray.crm.facade.request.personal.PersonalAuthStatusRequest;
import me.flyray.crm.facade.response.personal.PersonalInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * @Author: bolei
 * @date: 20:44 2019/2/15
 * @Description: 用户分销关系逻辑处理服务
 */

@FeignClient(value = "flyray-crm-core")
public interface PersonalBaseServiceClient {

    /**
     *  用户邀请关系处理
     * @param request
     * @return
     */
    @RequestMapping(value = "feign/distribution/invite",method = RequestMethod.POST)
    BaseApiResponse invite(@RequestBody DistributionInviteRequest request);

    /**
     *  用户授权状态查询
     * @param request
     * @return
     */
    @RequestMapping(value = "feign/personal/queryAuthStatus",method = RequestMethod.POST)
    BaseApiResponse<PersonalInfo> queryAuthStatus(@RequestBody @Valid PersonalAuthStatusRequest request);

    /**
     *  查询用户信息
     * @param request
     * @return
     */
    @RequestMapping(value = "feign/personal/info",method = RequestMethod.POST)
    BaseApiResponse<PersonalInfo> queryInfo(@RequestBody @Valid PersonalBaseRequest request);

    /**
     *  第三方授权授权,及基础信息授权注册
     * @param request
     * @return
     */
    @RequestMapping(value = "feign/personal/authRegister",method = RequestMethod.POST)
    BaseApiResponse personal3rdAuthRegister(@RequestBody @Valid Personal3rdAuthRegisterRequest request);

    /**
     *  第三方授权授权,及基础信息授权注册
     * @param request
     * @return
     */
    @RequestMapping(value = "feign/personal/update",method = RequestMethod.POST)
    BaseApiResponse updatePersonalInfo(@RequestBody PersonalBaseRequest request);

}
