package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("微信小程序获取邀请二维码")
public class WechatMiniProgramInviteQrParam implements Serializable {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="商户不能为空")
	@ApiModelProperty("商户编号") 
	private String merchantId;
	
	@NotNull(message="会员号")
	@ApiModelProperty("会员号")
	private String customerId;
	
}
