package me.flyray.gate.filter;

import me.flyray.auth.client.jwt.ServiceAuthUtil;
import me.flyray.auth.common.config.ServiceAuthConfig;
import me.flyray.gate.utils.FilterUrlUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: bolei
 * @date: 13:42 2018/12/2
 * @Description: 类描述
 */

@Component
@Slf4j
public class ServiceClientTokenHandler extends ZuulFilter {

    @Autowired
    private ServiceAuthConfig serviceAuthConfig;
    @Autowired
    private ServiceAuthUtil serviceAuthUtil;
    @Autowired
    private FilterUrlUtils filterUrlUtils;
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 3;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        // 申请客户端密钥头
        // 获取微服务client授权token 以便请求其他服务时可以通过验证
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader(serviceAuthConfig.getTokenHeader(), serviceAuthUtil.getClientToken());
        return null;
    }
}
