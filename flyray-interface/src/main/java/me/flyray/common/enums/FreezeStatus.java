package me.flyray.common.enums;

/**
 * @Author: bolei
 * @date: 11:26 2019/1/10
 * @Description: 出入账标识
 */

public enum FreezeStatus {

    freeze("1","已冻结"),
    feeze_part("2","部分冻结"),
    unfeeze("3","部分冻结");
    private String code;
    private String desc;

    private FreezeStatus(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static FreezeStatus getCommentModuleNo(String code){
        for(FreezeStatus o : FreezeStatus.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
